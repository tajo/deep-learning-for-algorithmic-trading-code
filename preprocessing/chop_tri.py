'''
   @author Vojtech Miksu <vojtech@miksu.cz>

   Fake datasets - trigonometric functions
   pre-process raw data into the datasets prepared for training, validating and testing
'''

from chop import chop

sets = ['sin_cos', 'sin_cos_tan']

for fnc in sets:
   params = {}
   params['inputfile'] = '../datasets/raw/'+fnc+'.csv'
   params['outputfile_train'] = '../datasets/output-'+ fnc +'-train.csv'
   params['outputfile_valid'] = '../datasets/output-'+ fnc +'-valid.csv'
   params['outputfile_test'] = '../datasets/output-'+ fnc +'-test.csv'
   params['normalize'] = False
   params['selected_attr'] = ['target',
                              'x',
                              'y']
   params['split_ratio'] = 0.85
   params['delimiter_in'] = ','
   params['delimiter_out'] = ','
   params['history_steps'] = 0
   params['history_column'] = ''
   params['predict_column'] = ''
   params['shuffle'] = True
   params['shuffleFirst'] = False
   chop(params)