'''
   @author Vojtech Miksu <vojtech@miksu.cz>

   Company stock datasets
   pre-process raw data into the datasets prepared for training, validating and testing
'''

from chop import chop

stocks = ['AAPL', 'ADBE', 'AMZN', 'GOOG', 'INTC', 'MSFT', 'NFLX', 'ORCL', 'TSLA', 'YHOO']

for stock in stocks:
   params = {}
   params['inputfile'] = '../datasets/raw/'+ stock +'/all-raw.tsv'
   params['outputfile_train'] = '../datasets/output-'+ stock +'-train.csv'
   params['outputfile_valid'] = '../datasets/output-'+ stock +'-valid.csv'
   params['outputfile_test'] = '../datasets/output-'+ stock +'-test.csv'
   params['normalize'] = True
   params['normalize_blacklist'] = ['history1','history2','history3','history4','history5','predictDiff','predictClass', 'predictClass3']
   params['selected_attr'] = ['predictDiff',
                              'predictClass',
                              'predictClass3',
                              'history1',
                              'history2',
                              'history3',
                              'history4',
                              'history5',
                              stock+'_close',
                              stock+'_high',
                              stock+'_low',
                              stock+'_time',
                              stock+'_spred',
                              stock+'_vol',
                              stock+'_MA_close_BolBands(3)',
                              stock+'_MA_close_BolBands(5)',
                              stock+'_MA_close_BolBands(7)',
                              stock+'_MA_close_BolBands(21)',
                              stock+'_vol_mvAvg(3)',
                              stock+'_vol_mvAvg(5)',
                              stock+'_vol_mvAvg(7)',
                              stock+'_vol_mvAvg(21)',
                              stock+'_diff_close(3)',
                              stock+'_diff_close(5)',
                              stock+'_diff_close(7)',
                              stock+'_diff_close(21)',
                              stock+'_diff_vol(3)',
                              stock+'_diff_vol(5)',
                              stock+'_diff_vol(7)',
                              stock+'_diff_vol(21)',
                              stock+'-count',
                              stock+'-followers']
   for st in stocks:
      if st == stock:
         continue
      params['selected_attr'].append(st+'_close')
      params['selected_attr'].append(st+'_vol')
      params['selected_attr'].append(st+'_MA_close_BolBands(5)')

   params['split_ratio'] = 0.85
   params['delimiter_in'] = '\t'
   params['delimiter_out'] = ','
   params['history_steps'] = 5
   params['history_column'] = stock+'_close'
   params['predict_column'] = stock+'_close'
   params['shuffle'] = True
   params['shuffleFirst'] = False
   chop(params)