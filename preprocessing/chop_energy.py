'''
   @author Vojtech Miksu <vojtech@miksu.cz>

   Energy datasets
   pre-process raw data into the datasets prepared for training, validating and testing
'''

from chop import chop

filename = 'energy'
params = {}
params['inputfile'] = '../datasets/raw/energy.csv'
params['outputfile_train'] = '../datasets/output-'+ filename +'-train.csv'
params['outputfile_valid'] = '../datasets/output-'+ filename +'-valid.csv'
params['outputfile_test'] = '../datasets/output-'+ filename +'-test.csv'
params['normalize'] = True
params['normalize_blacklist'] = ['cat']
params['selected_attr'] = ['cat',
                           'voltage',
                           'current',
                           'frequency',
                           'dpf',
                           'apf',
                           'realpower',
                           'realenergy',
                           'reactivepower',
                           'reactiveenergy',
                           'apparentpower',
                           'apparentenergy'
                           ]

params['split_ratio'] = 0.85
params['delimiter_in'] = ','
params['delimiter_out'] = ','
params['history_steps'] = 0
params['history_column'] = ''
params['predict_column'] = ''
params['shuffle'] = True
params['shuffleFirst'] = True
chop(params)