###
#
# Make UNE (unmetered loads) soft-meter CSV data file
# Copyright (C) 2013 Stephen Makonin. All Rights Reserved.
#
# READ: copy this file into you AMPds dataset directory
#
###

import sys, csv

# location of the dataset
ds_dir = 'AMPds/electricity'
# the header line of the CSV file
csv_hdr = 'cat,catname,timestamp,voltage,current,frequency,dpf,apf,realpower,realenergy,reactivepower,reactiveenergy,apparentpower,apparentenergy'
# the load ID of the house mains
mains_id = 'WHE'
# you can remove loads that you are not interested in disaggregating
load_ids = ['B1E', 'B2E', 'BME', 'CDE', 'CWE', 'DNE', 'DWE', 'EBE', 'EQE', 'FGE', 'FRE', 'GRE', 'HPE', 'HTE', 'OFE', 'OUE', 'TVE', 'UTE', 'WOE']


def load_csv(filename):
    print 'Loading file:', filename
    fp = open(filename, 'r')
    csvreader = csv.reader(fp)
    csvreader.next()
    data = [[float(col) for col in row] for row in csvreader]
    fp.close()
    return data

def save_csv(filename, data):
    print 'Saving file:', filename
    fp = open(filename, 'w')
    fp.write(csv_hdr + '\n')
    for d in data:
        if d:
            fp.write(','.join([str(a) for a in d]) + '\n')
    fp.close()

### 1. load in the mains then subtract all the other loads from it


idcko = 0
data = []
for load_id in load_ids:
    in_filename = 'AMPds/electricity/%s.csv' % (load_id)
    load = load_csv(in_filename)
    for i in xrange(20000):
        load[i][0:0] = [load_id]
        load[i][0:0] = [idcko]
        data.append(load[i])

    idcko+= 1
### 2. save the data to a CSV file

csv_filename = 'energy.csv'
save_csv(csv_filename, data)

print
print '...DONE!'
print
