import math
import random
import csv


SAMPLE_SIZE = 15000

data = []
sin = []
for i in xrange(SAMPLE_SIZE):
	rnd = random.uniform(-1,1)
	sin.append([0, rnd, math.sin(rnd)])

cos = []
for i in xrange(SAMPLE_SIZE):
	rnd = random.uniform(-1, 1)
	cos.append([1 ,rnd, math.cos(rnd)])

tan = []
for i in xrange(SAMPLE_SIZE):
	rnd = random.uniform(-1, 1)
	tan.append([2 ,rnd, math.tan(rnd)])


for i in xrange(SAMPLE_SIZE):
	data.append(sin[i])
	data.append(cos[i])
	data.append(tan[i])

random.shuffle(data)
data.insert(0, ['target', 'x', 'y'])

with open('sin_cos_tan.csv', 'wb') as csvfile:
	writer = csv.writer(csvfile, delimiter=',')
	writer.writerows(data)


data = []
for i in xrange(SAMPLE_SIZE):
	data.append(sin[i])
	data.append(cos[i])

random.shuffle(data)
data.insert(0, ['target', 'x', 'y'])

with open('sin_cos.csv', 'wb') as csvfile:
	writer = csv.writer(csvfile, delimiter=',')
	writer.writerows(data)


