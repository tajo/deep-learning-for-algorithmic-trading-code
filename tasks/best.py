{
	"tasks": [
		{
			"datasets" : ["INTC"],
			"target_name" : "predictClass",
			"finetune_lr" : 0.02,
			"pretraining_epochs" : 60,
			"pretrain_lr" : 0.001,
			"training_epochs" : 1000,
			"batch_size" : 1,
			"n_ins" : 56,
			"n_outs" : 2,
			"hidden_layers_sizes" : [85, 85, 85],
			"corruption_levels" : [0.1, 0.2, 0.3],
			"logfile" : "logs/log_test.csv"
		}
	]
}
