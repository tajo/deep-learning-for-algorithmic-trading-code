import numpy as np
import matplotlib.pyplot as plt
import csv
import random



def load_csv_data(path, delim):
	try:
		f_csv_in = open(path)
	except:
		print 'file ' + path + ' does not exist'
		sys.exit(2)

	print 'file ' + path + ' successfully loaded'
	f_csv_in = csv.reader(f_csv_in, delimiter=delim)
	data = [row for row in f_csv_in]
	return data[:1000]


font = {'family' : 'serif',
        'color'  : 'darkred',
        'weight' : 'normal',
        'size'   : 12,
        }

data = load_csv_data('datasets/output-sin_cos_tan-test.csv', ',')

data = zip(*data)

x = data[1][1:]
y = data[2][1:]


plt.plot(x, y, 'ro')

plt.xlabel('x', fontdict=font)
plt.ylabel('y', fontdict=font)
#plt.show()
plt.savefig('plot_tri.pdf')