import numpy as np
import matplotlib.pyplot as plt
import csv
import random
import sys
import math
from libs import utils


stocks = ["AAPL", "ADBE", "AMZN", "GOOG", "INTC", "MSFT", "NFLX", "ORCL", "TSLA", "YHOO"]


for stock in stocks:
	data = utils.load_csv_data('datasets/raw/'+stock+'/all-raw.tsv', '\t')
	sum = 0
	for row in data[1:]:
		sum += float(row[3])

	avg = sum/(len(data)-1)

	print stock + ' avg is {}'.format(avg)

	sum = 0
	for row in data[1:]:
		sum += (avg-float(row[3]))**2

	var = math.sqrt(sum/(len(data)-1))

	print stock + ' std deviation is {}'.format(var)