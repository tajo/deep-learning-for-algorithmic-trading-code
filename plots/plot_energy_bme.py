import numpy as np
import matplotlib.pyplot as plt
import csv
import random


stock = 'AMZN'


def load_csv_data(path, delim):
	try:
		f_csv_in = open(path)
	except:
		print 'file ' + path + ' does not exist'
		sys.exit(2)

	print 'file ' + path + ' successfully loaded'
	f_csv_in = csv.reader(f_csv_in, delimiter=delim)
	data = [row for row in f_csv_in]
	return data


font = {'family' : 'serif',
        'color'  : 'darkred',
        'weight' : 'normal',
        'size'   : 12,
        }

data = load_csv_data('datasets/raw/energy.csv', ',')
data = data[41285:41385] #BME


x = range(1, len(data)+1)
print len(x)

ziped = zip(*data)
y_voltage = map(float, ziped[3])
y_current = map(float, ziped[4])
y_frequency = map(float, ziped[5])
y_dpf = map(float, ziped[6])
y_apf = map(float, ziped[7])
y_realpower = map(float, ziped[8])
y_realenergy = map(float, ziped[9])

print len(y_voltage)

fig, ax = plt.subplots()
ax.set_yscale('log')

plt.plot(x, y_voltage, '-')
plt.plot(x, y_current, '--')
plt.plot(x, y_frequency, '-.')
#plt.plot(x, y_realpower)
#plt.plot(x, y_realenergy)

plt.legend(['voltage', 'current', 'frequency', 'DPF', 'APF', 'Real Power', 'Real Energy'])

# Tweak spacing to prevent clipping of ylabel
plt.subplots_adjust(left=0.15)
#plt.show()
plt.savefig('plot_energy_bme.pdf')