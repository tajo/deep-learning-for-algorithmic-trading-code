import numpy as np
import matplotlib.pyplot as plt
import csv
import random


stock = 'AAPL'


def load_csv_data(path, delim):
	try:
		f_csv_in = open(path)
	except:
		print 'file ' + path + ' does not exist'
		sys.exit(2)

	print 'file ' + path + ' successfully loaded'
	f_csv_in = csv.reader(f_csv_in, delimiter=delim)
	data = [row for row in f_csv_in]
	return data

font = {'family' : 'serif',
        'color'  : 'darkred',
        'weight' : 'normal',
        'size'   : 12,
        }

data = load_csv_data('datasets/raw/'+stock+'/all-raw.tsv', '\t')
x = range(1, len(data))

ziped = zip(*data)
y = map(float, ziped[3][1:])
y2 = map(float, ziped[30][1:])


plt.plot(x, y)
#plt.plot(x, y2)

plt.xlabel('time (min)', fontdict=font)
plt.ylabel('close price ($)', fontdict=font)


# Tweak spacing to prevent clipping of ylabel
plt.subplots_adjust(left=0.15)
#plt.show()
plt.savefig('plot_apple.pdf')r