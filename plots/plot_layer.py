"""
Demo of spines offset from the axes (a.k.a. "dropped spines").
"""
import numpy as np
import matplotlib.pyplot as plt


fig, ax = plt.subplots()

image = [[0, 2, 0], [-2, 0, -1], [0, 1, 0]]

ax.imshow(image, cmap=plt.cm.gray, interpolation='nearest')
ax.set_xticklabels([])
ax.set_yticklabels([])
plt.savefig('sample_vis.pdf')
#plt.show()